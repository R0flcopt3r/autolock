# autolock

This is a systemd service to lock the screen when sleep is initialized.

Add it to `/etc/systemd/` and use `systemd-analyze verify /etc/system/lock.service` and `systemctl enable /etc/systemd/lock.service`.

It will not autocomplete before the systemctl enable command as been run.

## Dependencies

This service uses my own i3lock script which can be found at [R0flcopt3r/Lockscreen-i3lock](https://github.com/R0flcopt3r/Lockscreen-i3locki)

it also depends on a service called `systemd-logind.service` which hopefull should be enabled by default on your system run `systemctl status systemd-logind.service` to verify.

